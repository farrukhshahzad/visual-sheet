'use strict';

angular.module('Authentication', []);
//var app = angular.module('myApp', ['ngRoute']);

var app = angular.module('myApp', [
    'Authentication',
    'ngRoute', 'ngAnimate',
    'ngCookies'
]);

var logged_status = null;
var logged_id = 0;

app.factory("services", ['$http', '$cookieStore', '$rootScope',
    function($http, $cookieStore, $rootScope) {
        var serviceBase = 'services/';
        var obj = {};

        obj.getAccountId = function () {
            logged_id = 0;
            if ($rootScope.globals.currentUser && $rootScope.globals.currentUser.user)
                logged_id = $rootScope.globals.currentUser.user.id;
            return logged_id
        };

        obj.getAccountInfo = function () {
            var user = null;
            if ($rootScope.globals.currentUser && $rootScope.globals.currentUser.user)
                user = $rootScope.globals.currentUser.user;
            return user
        };

        obj.login = function (user) {
            return $http.post(serviceBase + 'login', user).then(function (results) {
                logged_id = 0;
                if (results.status==200)
                    logged_id = results.data['id'];
                localStorage.setItem("logged_id", logged_id);
                console.log("***", obj.getAccountInfo(), results.data);
                return results.data || null;
            });
        };

        obj.getCustomers = function(){
            return $http.get(serviceBase + 'customers');
        };
        obj.getCustomer = function(customerID){
            return $http.get(serviceBase + 'customer?id=' + customerID);
        };

        obj.insertCustomer = function (customer, callback) {
            return $http.post(serviceBase + 'insertCustomer', customer).then(function (results) {
                callback(results);
                return results;
            });
        };

        obj.updateCustomer = function (id, customer) {
            return $http.post(serviceBase + 'updateCustomer', {id:id, customer:customer}).then(function (status) {
                return status.data;
            });
        };

        obj.deleteCustomer = function (id) {
            return $http.delete(serviceBase + 'deleteCustomer?id=' + id).then(function (status) {
                return status.data;
            });
        };


        obj.getStudents = function(){
            return $http.get(serviceBase + 'students');
        };
        obj.getStudent = function(ID){
            return $http.get(serviceBase + 'student?id=' + ID);
        };

        obj.insertStudent = function (student, callback) {
            return $http.post(serviceBase + 'insertStudent', student).then(function (results) {
                callback(results);
                return results;
            });
        };

        obj.updatePassword = function (id, new_pass, user, callback) {
            return $http.post(serviceBase + 'updatePassword', {id:id, new_pass: new_pass, email: user['email'] }).
              then(function (results) {
                callback(results);
                return results;
              });
        };
        obj.forgotPassword = function (email, callback) {
            return $http.post(serviceBase + 'forgotPassword', {email: email }).
              then(function (results) {
                callback(results);
                return results;
              });
        };

        obj.resetPassword = function (email, reset) {
            return $http.get(serviceBase + 'resetPassword?email='+ email + '&reset=' + reset);
        };

//Heads
        obj.getHeads = function(){
            return $http.get(serviceBase + 'heads?account=' + obj.getAccountId());
        };

        obj.getHead = function(id){
            return $http.get(serviceBase + 'head?id=' + id);
        };

        obj.insertHead = function (head) {
            return $http.post(serviceBase + 'insertHead', head).then(function (results) {
                console.log(head, results);
                return results;
            });
        };

        obj.updateHead = function (id, head) {
            return $http.post(serviceBase + 'updateHead', {id:id, head:head}).then(function (status) {
                console.log(head, status.data);
                return status.data;
            });
        };

        obj.deleteHead = function (id) {
            return $http.delete(serviceBase + 'deleteHead?id=' + id).then(function (status) {
                return status.data;
            });
        };

        obj.disableHead = function (id) {
            return $http.delete(serviceBase + 'controlRecord?id=' + id + '&status=0&table=Head').then(function (status) {
                return status.data;
            });
        };

        obj.enableHead = function (id) {
            return $http.delete(serviceBase + 'controlRecord?id=' + id + '&status=1&table=Head').then(function (status) {
                return status.data;
            });
        };

        
//Sheets
        obj.getSheetsCount = function() {
            var table = 'Sheet';
            var where = 'where account_id=' + obj.getAccountId();
            return $http.get(serviceBase + 'getTableDataCount?where=' + where + '&table=' + table);
        };

        obj.getSheetsSummary = function() {
            var table = 'Sheet';
            var where = 'where account_id=' + obj.getAccountId();
            var order = "order by datetime desc";
            var fields = "id, account_id, name, description, datetime, status";
            return $http.get(serviceBase + 'getTableData?where=' + where + '&table=' + table + 
                    '&fields=' + fields + '&order=' + order);
        };        

        obj.getSheets = function(limit){
            return $http.get(serviceBase + 'sheets?account=' + obj.getAccountId() + '&limit='+limit);
        };

        obj.getSheetsFunds = function(date1, date2) {
            console.log(date1, date2);
            if (date1==null)
                return $http.get(serviceBase + 'sheets_funds?account=' + obj.getAccountId());
            else    
                return $http.get(serviceBase + 'sheets_funds?account=' + obj.getAccountId() + '&from_date='+date1 + '&to_date='+date2);
        };

        obj.getSheet = function(id){
            return $http.get(serviceBase + 'sheet?id=' + id + '&account=' + obj.getAccountId()) ;
        };

        obj.insertSheet = function (sheet) {
            return $http.post(serviceBase + 'insertSheet', sheet).then(function (results) {
                console.log(sheet, results);
                return results;
            });
        };

        obj.updateSheet = function (id, sheet) {
            return $http.post(serviceBase + 'updateSheet', {id:id, sheet:sheet}).then(function (status) {
                console.log(sheet, status.data);
                return status.data;
            });
        };

        obj.deleteSheet = function (id) {
            return $http.delete(serviceBase + 'deleteSheet?id=' + id).then(function (status) {
                return status.data;
            });
        };

        obj.controlSheet = function (id, status) {
            return $http.delete(serviceBase + 'controlRecord?id=' + id + '&status='+ status +'&table=Sheet')
                .then(function (status) {
                    return status.data;
                });
        };

        obj.disableSheet = function (id) {
            return $http.delete(serviceBase + 'controlRecord?id=' + id + '&status=0&table=Sheet').then(function (status) {
                return status.data;
            });
        };

        obj.enableSheet = function (id) {
            return $http.delete(serviceBase + 'controlRecord?id=' + id + '&status=1&table=Sheet').then(function (status) {
                return status.data;
            });
        };

        obj.deleteRecord = function (id, table) {
            return $http.delete(serviceBase + 'deleteRecord?id=' + id + '&table='+table).then(function (status) {
                console.log(table, id, status.data);
                return status.code;
            });
        };

        return obj;
    }]);

app.directive('bindFile', [function () {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs, ngModel) {
            el.bind('change', function (event) {
                ngModel.$setViewValue(event.target.files[0]);
                $scope.$apply();
                $scope.loadFile();
            });
            
            $scope.$watch(function () {
                return ngModel.$viewValue;
            }, function (value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);

app.directive("compareTo", function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
});

app.controller('listCtrl', function ($scope, services) {
    services.getCustomers().then(function(data){
        $scope.customers = data.data;
    });
});

app.controller('passwordCtrl', function ($scope, $rootScope, $location, $routeParams, services, user) {

    var customerID = services.getAccountId();
    $scope.error = "";
    $scope.title = "Change Password";
    var page = $location.path().split('/')[1];
    if (page=="reset-password") $scope.title = "Reset Password";

    $scope.show_form = true;
    if (user) {
        $scope.user = user.data;
        if ($scope.user.msg) { $scope.error = $scope.user.msg; $scope.show_form = false;}
    } else
        $scope.user = services.getAccountInfo();

    //console.log(customerID, $scope.user);
    $scope._id = customerID;

    $scope.email = "";
    $scope.new_password = "";
    $scope.password = "";
    $scope.dataLoading = false;

    $scope.changePassword = function() {
        //$location.path('/');
        console.log(customerID, $scope.new_password);
        $scope.dataLoading = true;
        services.updatePassword(customerID, $scope.new_password, $scope.user,  function(response) {
                if (response.status==204)
                    $scope.error = "Password change Failed!";
                else {
                    $scope.error = response.data["msg"];
                    $scope.show_form = false;
                }
            }
        );
        $scope.dataLoading = false;
        console.log($scope.error);
    };

    $scope.forgotPassword = function() {
        //$location.path('/');
        console.log( $scope.email);
        $scope.dataLoading = true;
        services.forgotPassword($scope.email, function(response) {
                console.log( response);
                $scope.error = response.data["msg"];
            }
        );
        $scope.dataLoading = false;
        console.log($scope.error);
    };
});

app.controller('editCtrl', function ($scope, $rootScope, $location, $routeParams, services, customer) {
    var customerID = services.getAccountId(); //($routeParams.customerID) ? parseInt($routeParams.customerID) : 0;
    
    $rootScope.title = (customerID > 0) ? 'Edit Account' : 'Add Account';
    $scope.buttonText = (customerID > 0) ? 'Update Account' : 'Add New Account';
    var original = customer.data;
    console.log(original);
    //original._id = customerID;
    $scope.customer = angular.copy(original);
    $scope._id = customerID;
    $scope.error = "";
    $scope.dataLoading = false;

    $scope.isClean = function() {
        return angular.equals(original, $scope.customer);
    };

    $scope.deleteCustomer = function(customer) {
        $location.path('/');
        if(confirm("Are you sure to delete customer Id: "+$scope.customer._id)==true)
            services.deleteCustomer(customer.customerNumber);
    };

    $scope.saveCustomer = function(customer) {
        $location.path('/');
        $scope.dataLoading = true;
        if (customerID <= 0) {
            //var result = services.insertCustomer(customer);
            services.insertCustomer(customer, function(response) {
                $scope.error = response.data["msg"];
                if (response.status==200) {
                    console.log("***", response);
                    //$location.path('/');
                } else {
                    console.log("---", response, response.data["msg"]);
                    $scope.dataLoading = false;
                }
            });
        }
        else {
            services.updateCustomer(customerID, customer);
        }
    };
});

app.controller('headCtrl', function ($scope, $rootScope, services) {
    services.getHeads().then(function(data){
        $scope.heads = data.data;
    });
});

app.controller('editHeadCtrl', function ($scope, $rootScope, $location, $routeParams, services, head) {
    var id = ($routeParams.id) ? parseInt($routeParams.id) : 0;

    $rootScope.title = (id > 0) ? 'Edit Fund' : 'Add Fund';
    $scope.buttonText = (id > 0) ? 'Update Fund' : 'Add New Fund';
    var original = head.data;
    //original._id = id;
    $scope.head = angular.copy(original);
    $scope._id = id;
    $scope.account_id = logged_id;
    console.log($scope.head);

    $scope.isClean = function() {
        return angular.equals(original, $scope.head);
    };

    $scope.deleteHead = function(head) {
        $location.path('/heads');
        if(confirm("Are you sure to delete Fund Id: "+$scope.head._id)==true)
            services.deleteHead(head.id);
    };

    $scope.saveHead = function(head) {

        $location.path('/heads');
        if (id <= 0) {
            head.account_id = $scope.account_id;
            services.insertHead(head);
        }
        else {
            services.updateHead(id, head);
        }
    };
});

app.controller('sheetCtrl', function ($scope,  services, page) {
	$('#sheets').attr('class', 'active');
	$('#funds').attr('class', '');
	$('#sheet-funds').attr('class', '');
	$('#reports').attr('class', 'dropdown');
	$('#account').attr('class', 'dropdown');

    $scope.gap = 5;    
    $scope.itemsPerPage = 100;
    $scope.pagedItems = [];
    $scope.currentPage = 0;
    $scope.records = 0;
    
    $scope.groupToPages = function (items) {
        $scope.pagedItems = [];
        
        for (var i = 0; i < items.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ items[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push(items[i]);
            }
        }
    };
    
    $scope.range = function (size,start, end) {
        var ret = [];        
        console.log(size,start, end);
                      
        if (size < end) {
            end = size;
            start = size-$scope.gap;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
         console.log(ret);        
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
            $scope.loadSheet();
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
            $scope.currentPage++;
            $scope.loadSheet();
        }
    };
    
    $scope.setPage = function () {
        $scope.currentPage = this.n;
        $scope.loadSheet();
    };

    $scope.parseDate = function(dt) {
                var arr = dt.split(/[- :]/);
                var date = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                return date;
    };

    $scope.toggle_pie = function() {
       $scope.pie_display = !$scope.pie_display
    };

    $scope.toggle_table = function() {
       $scope.table_display = !$scope.table_display
    };
            
	$scope.loadSheet = function () {        
        var limit = $scope.currentPage * $scope.itemsPerPage + "," + $scope.itemsPerPage;
        console.log(limit);
        services.getSheets(limit).then(function(data){
            console.log(data);
            $scope.sheets = data.data;
            //$scope.groupToPages($scope.sheets);
            $scope.pie_display = false;
            $scope.table_display = true;

            $scope.getTotal = function(key){
                var total = 0;
                //console.log($scope.sheets, key);
                for(data in $scope.sheets) {
                    if (parseInt($scope.sheets[data]['status']) != 0)
                        total += $scope.sheets[data][key];
                }
                return total;
            };
            $scope.sheetValue = function(key){
                var sheet_data = [];
                for(data in $scope.sheets) {
                    var item = $scope.sheets[data];
                    var val=0;
                    if (parseInt(item['status']) != 0) {
                        if (key == 'total')
                            val = item['cash'] + item['chq'] + item['cc'];
                        else
                            val = item[key];
                        sheet_data.push({name: item['name'], y: val, visible: val > 0});
                    }
                }
                return {data: sheet_data};
            };  
            var sheet_values = [];
            sheet_values.push($scope.sheetValue('cash'));
            sheet_values.push($scope.sheetValue('cc'));
            sheet_values.push($scope.sheetValue('chq'));
            sheet_values.push($scope.sheetValue('total'));
            var types = ['Cash', 'Credit Cards', 'Cheques', 'Total'];

    /*         var sheet_data = [];
            for(var i in types) {
                sheet_data.push({data: sheet_values[i].data,
                    name:  types[i],
                    ignoreHiddenPoint: true,
                    center: [160 + 320*(i%2), 25 + 200*(i/2>>0)],
                    size: '45%',
                    title: {
                        align: 'center',
                        style: { color: 'blue'},
                        format: '<b>{name}</b>',
                        verticalAlign: 'middle'
                    },
                    showInLegend: i==types.length-1
                }
                );
            }
            plotPie('sheet_piechart', "Income by Sheets", sheet_data); */
        });
    };

    
    if (page==1) {
        services.getSheetsCount().then(function(data) {
            console.log(data);
            $scope.records = data;
        });
        $scope.loadSheet();
    }
    
    if (page==2) {
        services.getSheetsSummary().then(function(data){
            console.log(data);
            $scope.sheets = data.data;        
        });        
    }
    
    
});

app.controller('sheetFundCtrl', function ($scope,  services, page) {

    $scope.set_datetime = function () {
        $.datetimepicker.setLocale('en');
        var dt = $scope.from_datetime;
        if (!dt) {
            var d = new Date();
            dt = d.valueOf();
        }
        //dt = dt.substring(0,10);
        $('#datetimepicker_dark1').datetimepicker({theme:'dark', value:dt})
    };

    $scope.set_datetime2 = function () {
        $.datetimepicker.setLocale('en');
        var dt = $scope.to_datetime;
        if (!dt) {
            var d = new Date();
            dt = d.valueOf();
        }
        //dt = dt.substring(0,10);
        $('#datetimepicker_dark2').datetimepicker({theme:'dark', value:dt})
    };

    $scope.getData = function (date1, date2) {
        services.getSheetsFunds(date1, date2).then(function(data) {
            console.log(data);
            $scope.funds = data.data;
            $scope.pie_display = true;
            $scope.pie_display2 = false;
            $scope.table_display = true;

            $scope.toggle_pie = function() {
            $scope.pie_display = !$scope.pie_display
            };

            $scope.toggle_table = function() {
            $scope.table_display = !$scope.table_display
            };

            $scope.toggle_pie2 = function() {
            $scope.pie_display2 = !$scope.pie_display2
            };

            $scope.parseDate = function(dt) {
                var arr = dt.split(/[- :]/);
                var d_t = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                return d_t;
            };
            $scope.getTotal = function(key){
                var total = 0;
                for(data in $scope.funds) {
                    total += $scope.funds[data][key];
                }
                return total;
            };
            $scope.fundValue = function(key){
                var fund_value = [];
                var fund_name = [];
                var fund_data = [];
                for(data in $scope.funds) {
                    var item = $scope.funds[data];
                    var val=0;
                    if (key=='total')
                        val = item['cash']+item['chq']+item['cc'];
                    else
                        val = item[key];
                    if (val>0) {
                        fund_value.push(val);
                        fund_name.push(item['name']);
                    }
                    fund_data.push({name: item['name'], y: val, visible: val>0});
                }
                return {values: fund_value, labels: fund_name, data: fund_data};
            };
            $scope.fundName = function(){
                var fund_name = [];
                for(data in $scope.funds) {
                    var item = $scope.funds[data];
                    fund_name.push(item['name']);
                }
                return fund_name;
            };

            $scope.fundPlot = function (plotDiv, fund_values, types) {
                var data_value = [];
                //console.log(fund_values);
                var len = fund_values.length;
                for(var i in fund_values) {
                        data_value.push({
                            values: fund_values[i].values,
                            labels: fund_values[i].labels,
                            domain: {x: [i/len, i/len + 0.95/len]},
                            hole: 0.4,
                            //sort:true,
                            pull: 0.0,
                            name: types[i],
                            textfont: {size: 10, color:'#000'},
                            insidetextfont: {size: 10, color:'#000'},
                            outsidetextfont: {size: 10, color:'#000'},
                            hoverinfo: 'label+value+percent+name',
                            textinfo:'value+percent',
                            type: 'pie'
                        });
                }
                var data = data_value;

                var annotations = [];
                for(var i in types) {
                    annotations.push({
                        font: {size: 18, color: '#0000ff', face: 'bold'},
                        showarrow: false, text: types[i], x: i/len + 0.1, y: 0.0
                    });
                }
                //console.log(annotations);
                var layout = {
                    annotations: annotations,
                    titlefont: {size: 20, color: '#ff0000'},
                    title: 'Income by Funds',
                    height: 500,
                    width: 800
                };

                Plotly.newPlot(plotDiv, data, layout);
            };
            //var fund_names = $scope.fundName();
            var fund_values = [];
            fund_values.push($scope.fundValue('cash'));
            fund_values.push($scope.fundValue('cc'));
            fund_values.push($scope.fundValue('chq'));
            fund_values.push($scope.fundValue('total'));
            var types = ['Cash', 'Credit<br>Cards', 'Cheques', 'Total'];

            $scope.fundPlot('Pie', fund_values, types);

            var fund_data = [];
            for(var i in types) {
                fund_data.push({data: fund_values[i].data,
                    name:  types[i],
                    ignoreHiddenPoint: true,
                    center: [160 + 320*(i%2), 25 + 200*(i/2>>0)],
                    size: '45%',
                    title: {
                        align: 'center',
                        style: { color: 'blue'},
                        format: '<b>{name}</b>',
                        verticalAlign: 'middle'
                    },
                    showInLegend: i==types.length-1
                }
                );
            }
            plotPie('piechart', "Income by Funds", fund_data);
        });
    }

    $scope.getFundData = function () {
        $scope.getData($scope.from_datetime.substr(0,10), $scope.to_datetime.substr(0,10));
    } 

    console.log(page);
    if (page==2) {
        $scope.set_datetime();
        $scope.set_datetime2();        
    } 
    else
    {
        $scope.getData(null, null);
    }

});

app.controller('editSheetCtrl', function ($scope, $rootScope, $location, $routeParams, services, sheet) {
    var id = ($routeParams.id) ? parseInt($routeParams.id) : 0;

    $rootScope.title = (id > 0) ? 'Edit Sheet' : 'Add Sheet';
    $scope.buttonText = (id > 0) ? 'Update Sheet' : 'Add New Sheet';
    var original = sheet.data;
    var path = $location.path().split('/');
    //console.log(path);
    $scope.sheet = angular.copy(original);
    $scope._id = id;
	
	if ($scope.sheet.receipt && document.getElementById('img_output'))
		document.getElementById('img_output').src = atob($scope.sheet.receipt);
	
    $scope.user = services.getAccountInfo();
    $scope.account_id = services.getAccountId();
    $scope.removed = false;
	$scope.file = null;
    $scope.pie_display = false;
    $scope.table_display = true;
	$scope.tab_index = 0;
	$scope.disable = "disabled";

	$scope.getImage = function (src) {
		return atob(src)
	};
  
    $scope.toggle_pie = function() {
        $scope.pie_display = !$scope.pie_display
    };
	
    $scope.setTab = function(id) {
        $scope.tab_index = id
    };	

    $scope.toggle_table = function() {
        $scope.table_display = !$scope.table_display
    };

    $scope.set_datetime = function () {
        $.datetimepicker.setLocale('en');
        var dt = $scope.sheet.datetime;
        if (!dt) {
            var d = new Date();
            dt = d.valueOf();
        }
        $('#datetimepicker_dark').datetimepicker({theme:'dark', value:dt})
    };

    $scope.set_datetime();


    $scope.isClean = function() {
        return (angular.equals(original, $scope.sheet) && $scope.file==null) ? true : false;
    };
    $scope.onTextClick = function ($event) {
        $event.target.select();
    };
    $scope.parseDate = function(dt) {
        var datetime = null;
        if (dt==null)
            datetime = new Date();
        else {
            var arr = dt.split(/[- :]/);
            datetime = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
        }
        return datetime;
    };
    $scope.showAmount=function(amt){
        if (amt>0) return amt; else '';
    };
    $scope.getHeadName = function(type){
        var name = '';
        for(var data in $scope.sheet.heads){
            var head = 1 + parseInt($scope.sheet.heads[data].type);
            //console.log(type, head);
            if (head >= type)
                name += $scope.sheet.heads[data].name + ' ';
        }
        return name;
    };   
    $scope.addCC=function(head_id){
        var cc = {"name":"", "cc_no":"", "amount": 0, "head_id": head_id};
        $scope.sheet.heads[head_id].cc.push(cc);
    };
    $scope.addChq=function(head_id){
        var chq = {"name":"", "check_no":"", "amount": 0, "head_id": head_id};
        $scope.sheet.heads[head_id].chq.push(chq);
    };
	
    $scope.ifChqorCCDesc = function(){
        var desc = false;
		for(var data in $scope.sheet.heads){
			//var head = 1 + parseInt($scope.sheet.heads[data].type);
			var item = $scope.sheet.heads[data];
			if (item)
				for(var i = 0; i < item.cc.length; i++){
					if (item.cc[i].name.length 
						|| item.cc[i].description.length 
						|| item.cc[i].cc_no.length) {
							desc = true;
							break;	
						}
				}
				for(var i = 0; i < item.chq.length; i++){
					if (item.chq[i].name.length 
						|| item.chq[i].description.length 
						|| item.chq[i].check_no.length) {
							desc = true;
							break;	
						}
				}				
		}
		var cls = "panel panel-primary" + (desc? " col-xs-4" : " col-xs-3");
		console.log(cls);
        return cls
    };	
	
    $scope.getCCTotal = function(head_id){
        var total = 0;
        var item = $scope.sheet.heads[head_id];
        if (item)
            for(var i = 0; i < item.cc.length; i++){
                total += 1 * (item.cc[i].amount || 0);
            }
        //sheet_data.push({name: $scope.sheet.heads[head_id].name, y: total, visible: total > 0});
        return total;
    };
    $scope.getChqTotal = function(head_id){
        var total = 0;
        var item = $scope.sheet.heads[head_id];
        if (item)
            for(var i = 0; i < item.chq.length; i++){
                total += 1 * (item.chq[i].amount || 0);
            }
        //sheet_data.push({name: $scope.sheet.heads[head_id].name, y: total, visible: total > 0});
        return total;
    };
    $scope.getChqCount = function(head_id){
        var count = 0;
        for(var i = 0; i < $scope.sheet.heads[head_id].chq.length; i++){
            if ($scope.sheet.heads[head_id].chq[i].amount > 0) count++;
        }
        return count;
    };
    $scope.getCCCount = function(head_id){
        var count = 0;
        for(var i = 0; i < $scope.sheet.heads[head_id].cc.length; i++){
            if ($scope.sheet.heads[head_id].cc[i].amount > 0) count++;
        }
        return count;
    };
    $scope.getTotal = function(head_id, cash_only) {
        var type = $scope.sheet.heads[head_id].type;
        //console.log(type);
        var net = $scope.getChqTotal(head_id) + $scope.getCCTotal(head_id);
        if (cash_only) net = 0;
        var item = $scope.sheet.heads[head_id];
        var bill_total = 0;
        if (item)
            bill_total = 1 * (item.cash.bill_1||0) +
                2 * (item.cash.bill_2||0) +
                5 * (item.cash.bill_5||0) +
                100 * (item.cash.bill_100||0) +
                50 * (item.cash.bill_50||0) +
                20 * (item.cash.bill_20||0) +
                10 * (item.cash.bill_10||0) +
                1 * (item.cash.other_amount||0);

        //console.log(head_id, bill_total, net);
        //if (!all && type==1) bill_total = 0;
        return bill_total + net;
    };
    $scope.getGrandTotal = function(cash_only, type) {
        var total = 0;
        for(var data in $scope.sheet.heads){
            var head = 1 + parseInt($scope.sheet.heads[data].type);
            //console.log(type, head);
            if (head & type)
                total += $scope.getTotal(data, cash_only);
        }
        return total;
    };
    $scope.getGrandCCTotal = function(type) {
        var total = 0;
        for(var data in $scope.sheet.heads){
            var head = 1 + parseInt($scope.sheet.heads[data].type);
            if (head & type)
                total += $scope.getCCTotal(data);
        }
        return total;
    };
    $scope.getGrandChqTotal = function(type) {
        var total = 0;
        for(var data in $scope.sheet.heads){
            var head = 1 + parseInt($scope.sheet.heads[data].type);
            if (head & type)
                total += $scope.getChqTotal(data);
        }
        return total;
    };
    $scope.getGrandCCCount = function(type) {
        var total = 0;
        for(var data in $scope.sheet.heads){
            var head = 1 + parseInt($scope.sheet.heads[data].type);
            if (head & type)
              total += $scope.getCCCount(data);
        }
        return total;
    };
    $scope.getGrandChqCount = function(type) {
        var total = 0;
        for(var data in $scope.sheet.heads){
            var head = 1 + parseInt($scope.sheet.heads[data].type);
            if (head & type)
                total += $scope.getChqCount(data);
        }
        return total;
    };
    $scope.toggleCC = function(head_id) {
        $scope.sheet.heads[head_id].cc_display = !$scope.sheet.heads[head_id].cc_display;
    };
    $scope.toggleChq = function(head_id) {
        $scope.sheet.heads[head_id].chq_display = !$scope.sheet.heads[head_id].chq_display;
    };
    $scope.deleteSheet = function(sheet) {
        $location.path('/sheets');
        if (confirm("Are you sure to delete Sheet number: "+$scope.sheet._id)==true)
            services.deleteSheet(sheet.id);
    };
    $scope.deleteRecord = function(data, index) {
        var table = "CreditCard";
        $scope.removed = true;
        console.log(data);
        if ('check_no' in data) {
            table = "Cheque";
            $scope.sheet.heads[data.head_id].chq.splice(index,1);
        }
        else
            $scope.sheet.heads[data.head_id].cc.splice(index,1);
        //if (data.id > 0) {   
        //    services.deleteRecord(data.id, table);            
        //}
    };
    $scope.saveSheet = function(sheet) {
        $location.path('/sheets');
        console.log("----", sheet);
		$scope.loadFile();
        if (id <= 0) {
            sheet.account_id = $scope.account_id;
            //console.log("*-*-*-", sheet);
            services.insertSheet(sheet);
        }
        else {
            var update = !$scope.removed;
            if ($scope.removed)
            {
                if (confirm("Some records are deleted. Do you want to continue?")==true)
                    update = true;
            }
            if (update)
                services.updateSheet(id, sheet);
        }
    };

    $scope.sheetValue = function(key){
        var sheet_data = [];
        for(id in $scope.sheet.heads) {
            var item = $scope.sheet.heads[id];
            var val=0;
            if (key == 'total')
                val = $scope.getTotal(id, false);
            else if (key == 'cash')
                val = $scope.getTotal(id, true);
            else if (key == 'chq')
                val = $scope.getChqTotal(id);
            else
                val = $scope.getCCTotal(id);

            sheet_data.push({name: item['name'], y: val, visible: val > 0});
        }
        return {data: sheet_data};
    };

    if (path[1]=='print-sheet') {
        console.log("here");
        var sheet_values = [];
        sheet_values.push($scope.sheetValue('cash'));
        sheet_values.push($scope.sheetValue('cc'));
        sheet_values.push($scope.sheetValue('chq'));
        sheet_values.push($scope.sheetValue('total'));
        var types = ['Cash', 'Credit Cards', 'Cheques', 'Total'];

        var sheet_data = [];
        for (var i in types) {
            sheet_data.push({
                    data: sheet_values[i].data,
                    name: types[i],
                    ignoreHiddenPoint: true,
                    center: [160 + 320 * (i % 2), 25 + 200 * (i / 2 >> 0)],
                    size: '45%',
                    title: {
                        align: 'center',
                        style: {color: 'blue'},
                        format: '<b>{name}</b>',
                        verticalAlign: 'middle'
                    },
                    showInLegend: i == types.length - 1
                }
            );
        }
        //console.log(sheet_data);
        plotPie('print_piechart', "Income by Funds", sheet_data);
    }

	$scope.loadFile = function () {

		var input, file, fr;
		if (typeof window.FileReader !== 'function') {
		  alert("The file API isn't supported on this browser yet.");
		  return;
		}
		input = document.getElementById('fileinput');
		if (!input) {
		  alert("Um, couldn't find the fileinput element.");
		}
		else if (!input.files) {
		  alert("This browser doesn't seem to support the `files` property of file inputs.");
		}
		else if (!input.files[0]) {
		  //alert("Please select a file before clicking 'Load'");
		  return;		  
		}
		else {
		  file = input.files[0];

		  fr = new FileReader();
		  fr.onload = receivedText;
		  fr.readAsDataURL(file);
		}

		function receivedText(e) {
		  var dataURL = e.target.result;
		  //console.log(dataURL, file);
		  $scope.sheet.receipt = btoa(dataURL);
		  var elem = document.getElementById('img_output');
		  if (elem)
			elem.src = dataURL;
		  //var data = JSON.parse(lines);
		  //parseData(data);
		}
    };

});

app.filter('trustAsResourceUrl', ['$sce', function ($sce) {
  return function (val) {
    return $sce.trustAsResourceUrl(val);
  };
}]);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/login', {
                title: 'Login',
                hideMenus: true,
                templateUrl: 'partials/login.html',
                controller: 'LoginController', //'loginCtrl',
                resolve: {
                    user: function(services, $route) {
                        return {'email': 'a', 'pwd': 'b'};
                    }
                }
            })
            .when('/account', {
                title: 'Customers',
                templateUrl: 'partials/customers.html',
                controller: 'listCtrl'
            })
            .when('/edit-customer', {
                title: 'Edit Account',
                templateUrl: 'partials/edit-customer.html',
                controller: 'editCtrl',
                resolve: {
                    customer: function(services, $route){
                        return services.getCustomer(services.getAccountId());
                    }
                }
            })
            .when('/update-customer', {
                title: 'Edit Account',
                templateUrl: 'partials/update-customer.html',
                controller: 'editCtrl',
                resolve: {
                    customer: function(services, $route){
                        return services.getCustomer(services.getAccountId());
                    }
                }
            })            
            .when('/change-password', {
                title: 'New Password',
                templateUrl: 'partials/new-password.html',
                controller: 'passwordCtrl',
                resolve: {
                    user: function(services, $route){ return null;}
                }
            })
            .when('/forgot-password', {
                title: 'Forgot Password',
                templateUrl: 'partials/forgot-password.html',
                controller: 'passwordCtrl',
                resolve: {
                    user: function(services, $route){ return null;}
                }
            })
            .when('/reset-password/:email/:reset', {
                title: 'Reset Password',
                templateUrl: 'partials/new-password.html',
                controller: 'passwordCtrl',
                resolve: {
                    user: function(services, $route) {
                        var email = $route.current.params.email;
                        var reset = $route.current.params.reset;
                        //console.log(email, reset);
                        return services.resetPassword(email, reset);
                    }
                }
            })
            .when('/heads', {
                title: 'Heads',
                data: {   requireLogin: true  },
                templateUrl: 'partials/heads.html',
                controller: 'headCtrl'
            })
            .when('/edit-head/:id', {
                title: 'Edit Head',
                templateUrl: 'partials/edit-head.html',
                controller: 'editHeadCtrl',
                resolve: {
                    head: function(services, $route) {
                        var id = $route.current.params.id;
                        console.log(id);
                        return services.getHead(id);
                    }
                }
            })
            .when('/del-head/:id', {
                resolve: {
                    head: function(services, $route, $location) {
                        var id = $route.current.params.id;
                        $location.path('/heads');
                        if (confirm("Are you sure to delete Head id: "+id)==true)
                            services.deleteHead(id);
                    }
                }
            })
            .when('/disable-head/:id', {
                resolve: {
                    head: function(services, $route, $location) {
                        var id = $route.current.params.id;
                        $location.path('/heads');
                        services.disableHead(id);
                    }
                }
            })
            .when('/enable-head/:id', {
                resolve: {
                    head: function(services, $route, $location) {
                        var id = $route.current.params.id;
                        $location.path('/heads');
                        services.enableHead(id);
                    }
                }
            })
            .when('/', {
                title: 'Sheets',
                templateUrl: 'partials/sheets.html',
                controller: 'sheetCtrl',
                resolve: {
                    page: function(services, $route){ return 1;}
                }                 
            })
            .when('/sheets-summary', {
                title: 'Sheets Summary',
                templateUrl: 'partials/sheets-summary.html',
                controller: 'sheetCtrl',
                resolve: {
                    page: function(services, $route){ return 2;}
                }                  
            })            
            .when('/sheets-funds', {
                title: 'Sheets Funds',
                templateUrl: 'partials/sheets-funds.html',
                controller: 'sheetFundCtrl',
                resolve: {
                    page: function(services, $route){ return 1;}
                }                  
            })
            .when('/sheets-funds-date', {
                title: 'Sheets Funds',
                templateUrl: 'partials/sheets-funds-date.html',
                controller: 'sheetFundCtrl',
                resolve: {
                    page: function(services, $route){ return 2;}
                }                
            })            
            .when('/sheets-funds-pie', {
                title: 'Sheets Funds Pie',
                templateUrl: 'partials/sheets-funds-pie.html',
                controller: 'sheetFundCtrl'
            })
            .when('/edit-sheet/:id', {
                //title: 'Edit Sheets',
                templateUrl: 'partials/edit-sheet.html',
                controller: 'editSheetCtrl',
                resolve: {
                    sheet: function(services, $route){
                        var id = $route.current.params.id;
						$('#sheets').attr('class', 'active disabled');
						$('#funds').attr('class', 'disabled');						
						$('#sheet-funds').attr('class', 'disabled');
						$('#reports').attr('class', 'dropdown disabled');
						$('#account').attr('class', 'dropdown disabled');						
						return services.getSheet(id);
                    }
                }
            })
            .when('/edit-sheet2/:id', {
                //title: 'Edit Sheets',
                templateUrl: 'partials/edit-sheet2.html',
                controller: 'editSheetCtrl',
                resolve: {
                    sheet: function(services, $route){
                        var id = $route.current.params.id;
                        return services.getSheet(id);
                    }
                }
            })			
            .when('/print-sheet/:id', {
                title: 'Print Sheets',
                templateUrl: 'partials/print-sheet.html',
                controller: 'editSheetCtrl',
                resolve: {
                    sheet: function(services, $route){
                        var id = $route.current.params.id;
                        return services.getSheet(id);
                    }
                }
            })
            .when('/print-sheet-summary/:id', {
                title: 'Print Sheets',
                templateUrl: 'partials/print-sheet-summary.html',
                controller: 'editSheetCtrl',
                resolve: {
                    sheet: function(services, $route){
                        var id = $route.current.params.id;
                        return services.getSheet(id);
                    }
                }
            })            
            .when('/control-sheet/:id/:status', {
                resolve: {
                    head: function(services, $route, $location) {
                        var id = $route.current.params.id;
                        var status = $route.current.params.status;
                        console.log(id, status);
                        $location.path('/sheets');
                        services.controlSheet(id, status);
                    }
                }
            })
            .when('/del-sheet/:id', {
                resolve: {
                    sheet: function(services, $route, $location) {
                        var id = $route.current.params.id;
                        $location.path('/sheets');
                        if (confirm("Are you sure to delete Sheet id: "+id)==true)
                            services.deleteSheet(id);
                    }
                }
            })                 
            .otherwise({
                redirectTo: '/'
            });
    }]);

app.run(['$location', '$rootScope',  '$cookieStore', '$http',
    function($location, $rootScope, $cookieStore, $http) {
        //keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        };
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            //logged_id = localStorage.getItem("logged_id");
            if ($location.path() !== '/login' && $location.path() !== '/edit-customer' &&
                !($location.path().indexOf('reset-password')!=-1) &&
                $location.path() !== '/forgot-password' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
            //$rootScope.title = current.$route.title;
        });
    }
]);

/*app.run(['$rootScope', '$location', '$cookieStore', '$http',
 function ($rootScope, $location, $cookieStore, $http) {
 // keep user logged in after page refresh
 //$rootScope.globals = $cookieStore.get('globals') || {};
 //if ($rootScope.globals.currentUser) {
 //    $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
 // }
 $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
 //$rootScope.title = current.$route.title;
 });
 //$rootScope.$on('$locationChangeStart', function (event, next, current) {
 // redirect to login page if not logged in
 //    if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
 //       $location.path('/login');
 //   }
 //});
 }]);*/
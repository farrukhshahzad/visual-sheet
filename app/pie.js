(function (Highcharts) {
    Highcharts.seriesTypes.pie.prototype.setTitle = function (titleOption) {
        var chart = this.chart,
            center = this.center || (this.yAxis && this.yAxis.center),
            labelBox,
            box,
            format;

        if (center && titleOption) {
            box = {
                x: chart.plotLeft + center[0] - 0.5 * center[2],
                y: chart.plotTop + center[1] - 0.5 * center[2],
                width: center[2],
                height: center[2]
            };

			format = titleOption.text || titleOption.format;
            format = Highcharts.format(format, this);

            if (this.title) {
                this.title.attr({
                    text: format
                });

            } else {
                this.title = this.chart.renderer.label(format)
                    .css(titleOption.style)
                    .add()
            }
            labelBBox = this.title.getBBox();
            titleOption.width = labelBBox.width;
            titleOption.height = labelBBox.height;
            this.title.align(titleOption, null, box);
        }
    };

    Highcharts.wrap(Highcharts.seriesTypes.pie.prototype, 'render', function (proceed) {
        proceed.call(this);
        this.setTitle(this.options.title);
    });

} (Highcharts));


function plotPie(piediv, title_text, data) {

    // Build the chart
    Highcharts.chart(piediv, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            options3d: {
                enabled: true,
                alpha: 30
            },
            type: 'pie'
        },
        credits: {
            enabled: false
        },
        legend: {
            margin: 2, padding: 5, y:15, layout: 'horizontal', align: 'center', verticalAlign: 'bottom',
            shadow: true, backgroundColor: '#FFFFFF',
            title: {
                style: {fontStyle: 'italic', fontSize: '10px'}
            }
        },
        title: {
            text: title_text,
            margin: 20, x: -10,
            align: 'center',
            style: {color:'red', fontSize: '24px', fontFamily: 'Arial', fontStyle: 'bold'}
        },
        tooltip: {
            pointFormat: '{series.name}: <b>${point.y}, {point.percentage:.1f} %</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                innerSize: '50%',
                depth: 30,
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        if (this.y != 0) {
                            return Math.round(this.percentage*10)/10 + '%';
                        } else {
                            return null;
                        }
                    },
                    distance: 10
                    //format: '{point.percentage:.1f} %',
                    //style: {
                    //    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    //},
                    //connectorColor: 'silver'
                }
            }
        },
        series: data
    }, function(chart) {

        $(chart.series[3].data).each(function(i, e) {
            e.legendGroup.on('click', function(event) {
                var legendItem=e.name;

                event.stopPropagation();

                $(chart.series).each(function(j,f){
                       $(this.data).each(function(k,z){
                           if(z.name==legendItem)
                           {
                               if(z.visible)
                               {
                                   z.setVisible(false);
                               }
                               else
                               {
                                   z.setVisible(true);
                               }
                           }
                       });
                });

            });
        });
    });
}

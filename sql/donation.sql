-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 18, 2016 at 08:18 AM
-- Server version: 5.5.49-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `visonics`
--

-- --------------------------------------------------------

--
-- Table structure for table `Account`
--

CREATE TABLE IF NOT EXISTS `Account` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `contact_person` varchar(50) DEFAULT NULL,
  `contact_person2` varchar(50) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `phone2` varchar(25) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `web` varchar(500) DEFAULT NULL,
  `user` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `readonly_password` varchar(20) DEFAULT NULL,
  `operator_password` varchar(20) DEFAULT NULL,
  `logo` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `allowed_ips` varchar(100) NOT NULL DEFAULT '.,',
  `app` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `user` (`user`)  
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Account for login and profile' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `LoginHistory` (
  `id` bigint(10) NOT NULL auto_increment,
  `user` varchar(25) NOT NULL,
  `name` varchar(50) NULL,
  `login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logoff_date` timestamp NULL,  
  `ip` varchar(100) NULL,
  `hostname` varchar(200) NULL,  
  `user_agent` varchar(200) NULL,  
  `status` VARCHAR( 50 ) NULL,
  `app` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),  
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Record Login History' AUTO_INCREMENT=1 ;

--
-- Table structure for table `Sheet`
--

CREATE TABLE `Sheet` (
  `id` bigint(11) NOT NULL auto_increment,
  `account_id` int(8) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(500) NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1',  
  PRIMARY KEY (`id`),  
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Record Sheet Info' AUTO_INCREMENT=1 ;


--
-- Table structure for table `Head`
--

CREATE TABLE `Head` (
  `id` bigint(8) NOT NULL auto_increment,
  `account_id` int(8) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(500) NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `sequence` tinyint(3) NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),  
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Record Head Info' AUTO_INCREMENT=1 ;

--
-- Table structure for table `Check`
--

CREATE TABLE `Cheque` (
  `id` bigint(10) NOT NULL auto_increment,
  `head_id` int(8) NOT NULL,
  `sheet_id` int(8) NOT NULL,
  `name` varchar(50)  NULL,
  `description` varchar(500) NULL,
  `check_no` varchar(20) NULL,  
  `amount` DECIMAL(12, 2) NOT NULL DEFAULT '0.0',  
  PRIMARY KEY (`id`),  
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Record Checks Info' AUTO_INCREMENT=1 ;

--
-- Table structure for table `CreditCard`
--

CREATE TABLE `CreditCard` (
  `id` bigint(10) NOT NULL auto_increment,
  `head_id` int(8) NOT NULL,
  `sheet_id` int(8) NOT NULL,
  `name` varchar(50) NULL,
  `description` varchar(500) NULL,
  `cc_no` varchar(20) NULL,  
  `amount` DECIMAL(12, 2) NOT NULL DEFAULT '0.0',  
  PRIMARY KEY (`id`),  
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Record C/C Info' AUTO_INCREMENT=1 ;

--
-- Table structure for table `Cash`
--

CREATE TABLE `Cash` (
  `id` bigint(10) NOT NULL auto_increment,
  `head_id` int(8) NOT NULL,
  `sheet_id` int(8) NOT NULL,
  `bill_1` int(6) NOT NULL DEFAULT '0',
  `bill_2` int(6) NOT NULL DEFAULT '0',
  `bill_5` int(6) NOT NULL DEFAULT '0',  
  `bill_10` int(6) NOT NULL DEFAULT '0',  
  `bill_20` int(6) NOT NULL DEFAULT '0',  
  `bill_50` int(6) NOT NULL DEFAULT '0',  
  `bill_100` int(6) NOT NULL DEFAULT '0',  
  `other_amount` DECIMAL(12, 2) NOT NULL DEFAULT '0.0',  
  PRIMARY KEY (`id`),  
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Record Cash/Bills Info' AUTO_INCREMENT=1 ;

--
-- Table structure for table `File`
--

CREATE TABLE IF NOT EXISTS `File` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(8) NOT NULL,
  `data` mediumtext NOT NULL,
  `name` varchar(50) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Save File Data' AUTO_INCREMENT=15 ;


--
-- Table structure for table `Students`
--

CREATE TABLE IF NOT EXISTS `Students` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `family_id` int(8) NOT NULL DEFAULT '1',
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT 'TX',
  `zip` varchar(20) DEFAULT NULL,
  `contact_person` varchar(50) DEFAULT NULL,
  `contact_person2` varchar(50) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `phone2` varchar(25) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `teacher` varchar(200) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `school` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Student Info' AUTO_INCREMENT=1 ;

--
-- Table structure for table `Fees`
--

CREATE TABLE IF NOT EXISTS `Fees` (
  `id` bigint(8) NOT NULL AUTO_INCREMENT,
  `family_id` int(8) NOT NULL,
  `payment_method` tinyint(2) NOT NULL DEFAULT '0',
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `check_no` varchar(20) DEFAULT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `month` tinyint(2) NOT NULL DEFAULT '1',
  `year` smallint(4) NOT NULL DEFAULT '2017',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Record Fees from Parents' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Materials` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `title2` varchar(200) NOT NULL,
  `authors` varchar(500) NOT NULL,
  `category` varchar(50) NOT NULL,
  `publisher` varchar(500) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `publication_date` varchar(50) DEFAULT NULL,
  `addition_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `meta_data` varchar(200) DEFAULT NULL,
  `format` varchar(50) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `tags` varchar(500) DEFAULT NULL,
  `link1_text` varchar(50) DEFAULT NULL,
  `link2_text` varchar(50) DEFAULT NULL,
  `link3_text` varchar(50) DEFAULT NULL,
  `link1_url` varchar(200) DEFAULT NULL,
  `link2_url` varchar(200) DEFAULT NULL,
  `link3_url` varchar(200) DEFAULT NULL,  
  `authencity` int(2) NOT NULL DEFAULT '10',
  `language` varchar(50) DEFAULT NULL,
  `source` varchar(100) NOT NULL,
  `web` varchar(500) DEFAULT NULL,
  `cover` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of all materials' AUTO_INCREMENT=1 ;


SELECT s.id, account_id, s.name, s.description, sum(other_amount + bill_1 + bill_5*5 + bill_10*10 +bill_20*20 + bill_50*50 + bill_100*100), sum(cc.amount) as credit
FROM Sheet s 
inner join Cash c on c.sheet_id = s.id 
inner join CreditCard cc on cc.sheet_id = s.id 
group by s.id;

SELECT s.id, account_id, s.name, s.description, sum(other_amount + bill_1 + bill_5*5 + bill_10*10 +bill_20*20 + bill_50*50 + bill_100*100), cc.amount, ch.amount 
FROM Sheet s 
inner join Cash c on c.sheet_id = s.id 
left join (SELECT sum(amount) as amount FROM CreditCard group by sheet_id) cc on sheet_id = s.id 
left join (SELECT sum(amount) as amount FROM Cheque group by sheet_id) ch on sheet_id = s.id 
group by s.id;

SELECT count(*) as cnt, sum(amount) as amt FROM CreditCard c
inner join Sheet s on s.id = c.sheet_id
where head_id=24 and status<>0
group by head_id
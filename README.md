# Visual Income Sheet

Visual Income Sheet is an interactive visual software tool to record and display daily, weekly or monthly income sheets. 
It utilizes latest web technologies, including HTML5, JavaScript and AngularJS, to visualize income sheet on web browsers and smart devices. 

The tool is particularly beneficial for small businesses (like retail stores) and organizations, who collect money (or donations) on regular basis. 
The tool allows record, display and print of income sheet under multiple configurable funds and have option for cash, check or credit card entries.
The application have option to generate summary or detailed monthly, quaterly or annual reports. 


* [Web](http://www.visonics.net/visual-income/)
<?

/* START of version upgrade paste of previously defined CONSTANTS from util.php3 - if you are installing for the first time then just ignore this line */

/* CONSTANT declarations - DO NOT CHANGE UPPERCASE CONSTANTS */
/* Change lowercase values ONLY */
/* values you may change are marked with the comment " - modify" */


define("TABLESCHOLARS", "Scholars" ); /* the Scholars info*/
define("TABLECONTACT", "contactInfo" ); /* the Scholar's contact info*/

define("TABLETEACHERS", "Teachers" ); /* the Teachers info*/
define("TABLESTUDENTS", "Students" ); /* the Students info*/
define("TABLEBOOKS", "Books" ); /* the Books info*/
define("TABLEREFS", "Biblio" ); /* the Refs info*/
define("TABLEHADITH", "Hadith" ); /* the Refs info*/
define("TABLEASABAH", "Asabah" ); /* the Asabah info*/
define("TABLEALTHKAT", "Althkat" ); /* the Althkat info*/
define("TABLETARIKH", "Tarikh" ); /* the Tarikhul Kabir info*/
define("TABLEMIZAN", "Mizan" ); /* the Lisan al-Mizan info*/
define("TABLEIBNSAD", "ibnSad" ); /* the Tabaqat ibn Sad info*/
define("TABLESEER", "Seer" ); /* the See A'lam al-nubala info*/

define("TABLETAHZEEB", "Tahzeeb" ); /* the Tahzeeb al-Tahzeeb info*/
define("TABLEMEZAN", "Mezan" ); /* the Mezan al-A'tadal info*/
define("TABLETAQRIB", "Taqrib" ); /* the Tahzeeb al-Tahzeeb info*/
define("TABLEKAMAL", "TadheebAlKamal" ); /* the Tahzeeb al-Kamal info*/

define("NARRATORS", "Narrators" ); /* the Narrator List*/
define("NAMES", "Names" ); /* the Narrator List*/

define("TABLESCHOLARSADD", "ScholarsAdd" );
define("TABLEBOOKSADD", "BooksAdd" );

define("INSTALLPATH", "http://muslimscholars.info"); /* The full path to and including your install directory - be sure to include a trailing slash like "/" - modify */
define("UPLOADPATH","upload/");  //path where poster are uploaded

/* Who you are */
define("WEBMASTER", "webmaster@muslim-scholars.info" ); /* The webmasters email address - modify */
define("REQUESTEMAIL", "request@satamaticsIsatM2M.com" ); /* The webmasters email address - modify */
define("SALESEMAIL", "nicksalvi@satamaticsusa.com" ); /* The webmasters email address - modify */
define("AGENTEMAIL", "farrukh@muslim-scholars.info" ); /* The webmasters email address - modify */
define("COMPANY", "Muslim-scholars" ); /* The name of your organization, used in page titles etc - modify */


define("ADDRESS", "4503 W. DeYoung St.  Suite 204C   Marion, IL 62959" );
define("SALESPHONE", "1-877-SAT-MATD or 301-560-4716 (outside US)" ); /* Your phone number for sales as on advertise.php3, when a record is added this will show - modify */
define("SALESFAX", "360-246-7263" ); /* Your Fax number for sales as on advertise.php3, when a record is added this will show - modify */
//define("SALESPHONE", "732-926-9729" ); /* Your phone number for sales as on advertise.php3, when a record is added this will show - modify */
//define("SALESFAX", "732-926-0132" ); /* Your Fax number for sales as on advertise.php3, when a record is added this will show - modify */
//define("ADDRESS", "156 Carlton Ave., Piscataway, NJ 08854." );


/* How your phpYellow Pages implementation works */
define( "SSL", "on"); /* whether or not you require SSL 3.0 validation on admin login */
define("EMAILONLISTING", "yes"); /* if you want the WEBMASTER notified of every new listing added or modified then this value should be "yes" - modify */
define("EMAILONCUSTOMER", "yes"); /* if you want the SALES notified of every new customer added or modified then this value should be "yes" - modify */
define("EMAILTOCUSTOMERONLISTING", "yes"); /* if you want the Customer notified of every new listing added or modified then this value should be "yes" - modify */
define("EMAILTOCUSTOMER", "yes"); /* if you want the Customer be notified of new Customer added or modified then this value should be "yes" - modify */
define("EMAILTOAGENTONLISTING", "yes"); /* if you want the Agent notified of every new listing added or modified then this value should be "yes" - modify */
define("EMAILTOAGENTONCUSTOMER", "yes"); /*if you want the Agent be notified of new Customer added or modified then this value should be "yes" - modify */
define("EMAILTOAGENT", "yes"); /*if you want the Agent be notified of new Customer added or modified then this value should be "yes" - modify */

define("YOURPHPYELLOWNAME", "satamaticsIsatM2M.com"); /* the name you call your implementation of phpYellow, used in html page titles and email - modify */
define("RECORDSPERPAGE", "20"); /* The number of results per page in any search - modify if wanted */

define("SHOWSQL", "no" ); /* DO NOT CHANGE. for debugging, choose to show the sql queries. Anything but "no" will cause display of sql in admin results only */

/* END of version upgrade paste of previously defined CONSTANTS from util.php3 - if you are installing for the first time then just ignore this line */


/* Database and connectivity */
//muslimscholars.db.5696160.hostedresource.com
define("DBNAME2", "dplus" ); /* your mySQL database name - modify */
define("DBPASSWORD2", "sat101" ); /* the password to get into your mysql database - modify */
define("USER2", "satamatics" ); /* the name of your mysql user, often made the same as your internet user account - modify */
define("HOSTSERVER2", "mysql"); /* the name of your database host server - modify */

//define("DBNAME", "dplus" ); /* your mySQL database name - modify */
//define("DBNAME_BACKUP", "Backup" ); /* your mySQL database name - modify */
//define("DBPASSWORD", "sat201" ); /* the password to get into your mysql database - modify */
//define("USER", "satamati" ); /* the name of your mysql user, often made the same as your internet user account - modify */
//define("HOSTSERVER", "localhost"); /* the name of your database host server - modify */

define("DBNAME", "msd" ); /* your mySQL database name - modify */
define("HOSTSERVER", "localhost"); /* the name of your database host server - modify */

define("DBPASSWORD", "MSdb@2011" ); /* the password to get into your mysql database - modify */
define("USER", "muslimscholars" ); /* the name of your mysql user, often made the same as your internet user account - modify */

//define("DBPASSWORD", "alcpme" ); /* the password to get into your mysql database - modify */
//define("USER", "root" ); /* the name of your mysql user, often made the same as your internet user account - modify */


/* Utility routines for MySQL from http://www.vtwebwizard.com - Thanks! */

class MySQL_class {
    var $db, $id, $result, $rows, $data, $a_rows, $errors, $errStr;
    var $user, $pass, $host;

    function Setup ($user, $pass) {
        $this->user = $user;
        $this->pass = $pass;
    }

    function Create ($db) {
        if (!$this->user) {
            $this->user = USER;
        }
        if (!$this->pass) {
            $this->pass = DBPASSWORD;
        }
        if (!$this->db) {
                $this->db = $db;
                }
        if (!$this->host) {
                $this->host = HOSTSERVER;
                }
        $this->id = @mysql_pconnect($this->host, $this->user, $this->pass) or
            $this->MySQL_ErrorMsg("Unable to connect to MySQL server: $this->host : '$SERVER_NAME'");
        $this->selectdb($db);
    }

    function Create2 ($db, $user, $password, $host) {
        if (!$this->user) {
            $this->user = $user;
        }
        if (!$this->pass) {
            $this->pass = $password;
        }

        if (!$this->host) {
            $this->host = $host;
        }
        $this->id = @mysql_pconnect($this->host, $this->user, $this->pass) or
            $this->MySQL_ErrorMsg("Unable to connect to MySQL server: $this->host : '$SERVER_NAME'");
        $this->selectdb($db);
    }

    function SelectDB ($db) {
        $this->errors = 0;
        $this->errStr = "Ok";
        @mysql_select_db($db, $this->id) or
            $this->MySQL_ErrorMsg ("Unable to select database: $db");
    }

    /* Use this function is the query will return multiple rows.  Use the Fetch
     routine to loop through those rows. */
    function Query ($query) {

        @mysql_query("SET CHARACTER_SET_RESULTS=NULL", $this->id);
        //echo $query;
        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Bad query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
    }

    function Query2 ($query) {

        //@mysql_query("SET CHARACTER_SET_RESULTS", $this->id);

        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Bad query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
    }

    /* Use this function if the query will only return a
    single data element. */
    function QueryItem ($query) {
        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Unable to perform query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
        $this->data = @mysql_fetch_array($this->result);
       //  or  $this->MySQL_ErrorMsg ("Unable to fetch data from query: $query");
        return($this->data[0]);
    }

    /* This function is useful if the query will only return a
    single row. */
    function QueryRow ($query) {
        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Unable to perform query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
        $this->data = @mysql_fetch_array($this->result);
        // or    $this->MySQL_ErrorMsg ("Unable to fetch data from query: $query");
        return($this->data);
    }

    function Fetch ($row) {
        @mysql_data_seek($this->result, $row) or
            $this->MySQL_ErrorMsg ("Unable to seek data row: $row");
        $this->data = @mysql_fetch_array($this->result) or
            $this->MySQL_ErrorMsg ("Unable to fetch row: $row");
    }

    function Insert ($query) {
        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Unable to perform insert: $query");
        $this->a_rows = @mysql_affected_rows($this->result);
    }

    function Update ($query) {
        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Unable to perform update: $query");
        $this->a_rows = @mysql_affected_rows($this->result);
    }

    function Delete ($query) {
        $this->result = @mysql_query($query, $this->id) or
            $this->MySQL_ErrorMsg ("Unable to perform Delete: $query");
        $this->a_rows = @mysql_affected_rows($this->result);
    }

    /* ********************************************************************
     * MySQL_ErrorMsg
     *
     * Print out an MySQL error message
     *
     */

    function MySQL_ErrorMsg ($msg) {
        /* Close out a bunch of HTML constructs which might prevent
        the HTML page from displaying the error text. */
        //echo("</ul></dl></ol>\n");
        //echo("</table></script>\n");

        /* Display the error message */
        $this->errors = 1;
        $this->errStr = mysql_error();
        //$db_err = 1;
        $text  = "$msg ...<br>";
        $text .= $this->errStr."<br>";

        print($text);

        return($text);

    }

}


/* check for unwanted characters in user input query */
function verifyData ($query) {
        if($query){
                $query = str_replace ("%", "", $query);
                //$query = str_replace ("_", "", $query);
                $query = str_replace (">", "", $query);
                $query = str_replace ("<", "", $query);
                $query = str_replace ("|", "", $query);
                $query = str_replace ("`", "", $query);
                //$query = str_replace ("\\", "", $query); incompatible with addslashes
                $query = str_replace ("+", "", $query);
                $query = str_replace ("?", "", $query);
                $query = str_replace ("^", "", $query);
                return $query;
        }else{
                exit;
        }
}

function getRegionList($db,$sql, $region)
{
   if (strlen($sql)<5) return("");

   $result = mysql_query($sql,$db);

   if ($myrow = mysql_fetch_array($result)) { // display list if there are records to display
      do {
       $count++;
       $area=$myrow["RegionCode"];
       $dash="";
       if ($area==0 || $area==70 || $area==99) $dash="---";
       if ($area==$region)
         printf("<option value=%d selected>%s%s</option>\n",$area,$dash,$myrow["RegionName"]);
       else
         printf("<option value=%d>%s%s%s</option>\n",$area,$dash,$myrow["RegionName"],$dash);
      } while ($myrow = mysql_fetch_array($result));
      //printf("%d records selected\n",$count);
   }
   return($count);
}


function findInDB($sql2,$names, $arabic=0)
{
        GLOBAL $colors;

        $fId = trim(strtok($names , ","));
        $name = "";
        $comma = "";
        $comma2 = "<br>";
        while (strlen($fId)>0)
        {
            if ((string)$fId === (string)(int)$fId)
            {
                if ($arabic==1)
                  $sName = $sql2->QueryItem("select otherName from ".TABLESCHOLARS." where id='$fId'");
                else
                  $sName = $sql2->QueryItem("select famousName from ".TABLESCHOLARS." where id='$fId'");

                //$sName = $sql2->data[0];
                //$oName = $sql2->data[1];
                $rk = (int)($fId/10000) + 1;
                $color = $colors[$rk];
                if ($fId>=9990 && $fId<=9999)
                  $name .= $comma."<A href='manage.php?submit=scholar&ID=$fId' title=\"Goto $sName\"><font color=black>$sName</font></A>";
                else
                  $name .= $comma."<A href=manage.php?submit=scholar&ID=$fId title=\"Goto $sName\"><font color=$color>$sName</font></A>";
                //if (strlen($oName)>3)
                //  $name .= $comma2."<A href=manage.php?submit=scholarTree&ID=$fId title=\"Goto Family Tree of $sName\">$oName</A>";
            }
            else
            {
                $name .= $comma.$fId;
            }

            $fId = trim(strtok(","));
            $comma = ", ";
        }
        return($name);
}

function findInDB3($sql2,$names,$rank=-1,$arabic=0,$gap=", ")
{
        GLOBAL $colors;
        $fId = trim(strtok($names, ","));
        $name = "";
        $comma = "";
        $comma2 = "<br>";
        $count=0;
        $size="-1";
        if ($arabic==1) $size="+1";
        while (strlen($fId)>0)
        {
            if ((string)$fId === (string)(int)$fId)
            {
                $rk = (int)($fId/10000) + 1;
                if ($rk==$rank || $rank==-1)
                {
                    $color = $colors[$rk];
                    $sName = $sql2->QueryRow("select famousName,otherName,birthDate,deathDate,liveCity,tags,kunya from ".TABLESCHOLARS." where id='$fId'");
                    $nm = $sName[$arabic];
                    $fName = $sName[0];
                    $dob  = trim($sName[2]);
                    $dod  = trim($sName[3]);
                    $dbd ="";
                    if (strlen($dob)>2 && strlen($dod)>2) $dbd = "[$dob - $dod]";
                    else if (strlen($dod)>2) $dbd = "[d. $dod]";

                    $cities = trim($sName[4]);
                    $kunya = trim($sName[6]);
                    $cap = "$fName,$kunya $dbd [$cities]";
                    $name .= $comma."<A href=manage.php?submit=scholar&ID=$fId title=\"$cap\"><font color=$color size='size'>".$nm."</font></A>";
                    $comma = $gap;
                    $count++;
                    if ($count>5) break;
                }
            }
            $fId = trim(strtok(","));

        }
        return($name);
}

function getIds($names, $rank=-1)
{

        $fId = trim(strtok($names , ","));
        if (strlen($fId)==0) $fId = trim($names);
        $name = "";
        $comma = "";
        $comma2 = "<br>";
        while (strlen($fId)>0)
        {
            if ((string)$fId === (string)(int)$fId)
            {
                $rk = (int)($fId/10000) + 1;
                if ($rk==$rank || $rank==-1)
                {
                   $name .= $comma.$fId;
                   $comma=",";
                }
            }
            $fId = trim(strtok(","));
        }
        return($name);
}


function findInDB2($sql2,$names,$mID=0)
{
        GLOBAL $colors;

        $name = "";
        $fId = trim(strtok($names , ","));

        if ($fId=="")
          $name = "Unknown";
        $no=1;
        $comma = "";
        $comma2 = "<br>";
        while (strlen($fId)>0)
        {
            if ((string)$fId === (string)(int)$fId)
            {
                $dd = $sql2->QueryRow("select famousName,otherName,RefId,miscIds from ".TABLESCHOLARS." where id='$fId'");
                $sName = $dd[0];
                $oName = $dd[1];
                $idd = $dd[2]."-".$dd[3];
                $rk = (int)($fId/10000) + 1;
                $color = $colors[$rk];

                //$name .= $comma."<A href=manage.php?submit=scholar&ID=$fId>$sName</A>";
                if ($fId>=9990 && $fId<=9999)
                  $name .= $comma."<A href='manage.php?submit=scholar&ID=$fId' title=\"Goto $sName\"><font color=black>$sName</font></A>";
                else
                  $name .= $comma."<A href=manage.php?submit=scholar&ID=$fId title=\"Goto $sName\"><font color=$color>$sName</font></A>";
                if (strlen($oName)>3)
                  $name .= $comma2."<A href=manage.php?submit=scholarTree&ID=$fId title=\"Goto Family Tree of $sName\"><font color=$color>$oName</font></A>";
                if ($mID>0)
                  $name  .=  "<A href='manage.php?submit=scholarEdit2&ID=$fId&mID=$mID' title='$idd'>.</A>";
                //$name  .=  "<A href='manage.php?submit=scholarEdit2&ID=$fId'>.</A>";

            }
            else
            {
                $name .= $comma.$fId;
            }

            $fId = trim(strtok(","));
            $no++;
            $comma = "<br>$no)";
            //$comma2 = "<br>";
        }
        return($name);
}

function linkTags($tags)
{
        $fId = trim(strtok($tags, ","));
        $tag = str_replace(' ', '%20', $fId);
        $name = "";
        $comma = "";
        while (strlen($fId)>0)
        {
            $name .= $comma."<A href=manage.php?submit=Find&yfield=3&scholarSearch=$tag title=\"find all '$tag'..\">$fId</A>";
            $fId = trim(strtok(","));
            $tag = str_replace(' ', '%20', $fId);
            $comma = ", ";
        }
        return($name);
}

function drawBox($align,$size,$heading,$body)
{
                print("<table border='0' align=$align width='$size' cellpadding='2' class='headerDiv'>\n");
                print("<tr><th hieght='20px'><font color='white' size='-1'>$heading</font></th></tr>");
                print("<tr><td><table border='0' width='100%' cellpadding='1' align='center' class='form'>\n");
                print("<tr><td nowrap align=CENTER style=\"border: 1px solid black; padding: 0.2em;&#160;;\">$body</td></tr></table>\n</td></tr></table>\n");
}

function processEntry($sql2,$info, $width, $upto=0)
{
    $count=0;
    $chds="";
    $infos = explode(",",$info);
    foreach ($infos as $chd)
    {
             $chd = trim($chd);
             if (strlen($chd)<1) continue;
             if ((string)$chd === (string)(int)$chd)
             {
                 $child = $sql2->QueryItem("select famousName from ".TABLESCHOLARS." where id='$chd'");
                 $chd1 =$child;
                 if ($upto>0)
                 {
                     $parts = explode(" ",$child);
                     $child="";
                     for ($j = 0; $j < $upto; $j++)
                       $child .= $parts[$j]." ";
                 }
                 $chd = "<A class='tree' href='manage.php?submit=scholar&ID=$chd' title=\"$chd1\">".$child."</A>";
             }
             else
             {
               $child = $chd;
               if ($upto>0)
               {
                    $parts = explode(" ",$child);
                    $child="";
                    for ($j = 0; $j < $upto; $j++)
                       $child .= $parts[$j]." ";
                    if (count($parts)>$upto)
                      $chd = "<A class='tree' href='' title=\"$chd\">".$child."</A>";
               }

             }

             $chds .= $chd."<br>";
             $count++;
             $width=max($width,strlen($child));
    }
    $entry['count'] = $count;
    $entry['width'] = $width;
    $entry['child'] = $chds;
    return ($entry);
}

function findTeachers($sql2,$sId,$field,$arabic=0,$type=0)
{
        GLOBAL $colors;
        $name = "";
        $IDs="";
        $comma = "";
        $comma2 = "<br>";
        $count=0;

        $query = "select id, famousName,otherName from ".TABLESCHOLARS." where concat(',',$field,',') like '%,$sId,%' order by Id";

        $sql2->Query ("SET NAMES 'utf8'");
        $sql2->Query ($query);
        $rows =  $sql2->rows;

        for ($kk = 0; $kk < $rows; $kk++)
        {
            $sql2->Fetch($kk);
            $sid = $sql2->data[0];
            $rk = (int)($sid/10000) + 1;
            $color = $colors[$rk];

            $nm = $sql2->data[$arabic + 1];
            $name .= $comma."<A href=manage.php?submit=scholar&ID=$sid title=\"Goto $nm\"><font color=$color>".$nm."</font></A>";
            $IDs .=$comma.$sid;
            $comma = ",";
            $count++;
            //if ($count>5) break;
        }
        if ($type==0) return($name); else return($name."<br>".$IDs);
}


function birthDeath($dob, $dod, $rk, $tb)
{
                $fix="";
                $fix2="";

//DOB
                $dobh = trim(strtok($dob, "/"));
                $dobg = trim(strtok("*"));
                $dobg = trim(strtok($dobg, " "));

                $date[0]['G'] = $dobg;
                $date[0]['H'] = $dobh;

                //$tb = $sql->data[5];
                //$tb= $sql2->QueryItem("select tabqa from ".TABLETAQRIB." where scholarId=$sid");
                //if ($tb<4) $tb=0; else $tb -= 3;

                $fs = substr($dobh,0,1);

                if ($fs=="~" || $fs=="<" || $fs==">") $dobh = trim(substr($dobh,1));
                if ($fs=="a") $dobh = trim(substr($dobh,5));
                if ($fs=="b") $dobh = trim(substr($dobh,6));
                $dobh = trim(strtok($dobh, " "));
                $cbh = trim(strtok("*"));
                $sbe=1;
                $sb=1;

                $dobhe = 20 + 80 * ($rk-2);
                if ($tb>1) $dobhe = ($tb-1)*20 - 10;
                if ($rk==1) {$dobhe = -10; $sbe=1; }
                $date[0]['est']=0;


                if ($dobh==0 && $rk>1) {  $dobh = $dobhe ; $fix .="&laquo"; $date[0]['est']=1;}
                if ($dobh==0 && $rk==1) { $dobh = abs($dobhe); $cbh="BH"; $fix .="&laquo"; $date[0]['est']=1;}

                $signb = "";
                $signd = "";


                if (substr($cbh,0,2)=="BH") {$signb = "-"; $sb=-1;} else $cbh="AH";
                $jdb = (int)(islamicToJd($sb*$dobh, 6, 15)/365.25) - 4713;
                $jdbe = (int)(islamicToJd($sbe*$dobhe, 6, 15)/365.25) - 4713;

                if ($dobg==0) $dobg=$jdb;

                $dobg = sprintf("%04d",$dobg);
                $dobh = $signb.sprintf("%04d",$dobh);

                $date[0]['HA'] = $dobh;
                $date[0]['GA'] = $dobg;


                $date[0]['GC'] = $jdb;

                $date[0]['HE'] = $dobhe;
                $date[0]['GE'] = $jdbe;

//DOD
                $dodh = trim(strtok($dod, "/"));
                $dodg = trim(strtok("*"));
                $dodg = trim(strtok($dodg, " "));

                $date[1]['G'] = $dobg;
                $date[1]['H'] = $dobh;

                $fs = substr($dodh,0,1);

                if ($fs == "~" || $fs == "<" || $fs == ">") $dodh = trim(substr($dodh,1));
                if ($fs == "a") $dodh = trim(substr($dodh,5));
                if ($fs == "b") $dodh = trim(substr($dodh,6));
                //$fs = $dodh;

                $dodh = trim(strtok($dodh, " "));
                $cdh = trim(strtok("*"));
                $sd=1;
                $sde=1;

                if (substr($cdh,0,2)=="BH") {$signd = "-"; $sd=-1;} else $cdh="AH";

                $dodhe = 40 + 80 * ($rk-1);
                if ($tb>0) $dodhe = 60 + ($tb-1)*20;

                $date[1]['est']=0;

                if ($dodh==0 && $rk>0 && $signd=="") { $dodh = $dodhe; $fix2 .="&raquo"; $date[1]['est']=1;}


                $jdd = (int)(islamicToJd($sd*$dodh, 6, 15)/365.25) - 4713;
                $jdde = (int)(islamicToJd($sde*$dodhe, 6, 15)/365.25) - 4713;

                if ($dodg<$dobg) $dodg=$jdd;
                $dodg = sprintf("%04d",$dodg);
                $dodh = $signd.sprintf("%04d",$dodh);

                $date[1]['HA'] = $dodh;
                $date[1]['GA'] = $dodg;


                $date[1]['GC'] = $jdd;

                $date[1]['HE'] = $dodhe;
                $date[1]['GE'] = $jdde;

                $dbda = "[".abs($date[0]['HA'])." $cbh/".$date[0]['GA']." CE - ".abs($date[1]['HA'])." $cdh/".$date[1]['GA']." CE] (Cal.)";

                $date[0][0] = $dbda;

                return($date);

}

function utf8_strrev($str){
    preg_match_all('/./us', $str, $ar);
    return join('',array_reverse($ar[0]));
}

function islamicToJd($year, $month, $day)
{
       $_ISLAMIC_EPOCH = 1948439.5;
       return($day + ceil(29.5 * ($month - 1)) + ($year - 1) * 354 + (int)((3 + (11 * $year)) / 30) + $_ISLAMIC_EPOCH) - 1;
}

class ArDate
{
    protected $mode = 1;
    protected $xml  = null;

    protected static $islamicEpoch = 1948439.5;

    /**
     * "date" method output charset
     * @var String
     */
    public $dateOutput = 'utf-8';

 /**
     * Convert given Gregorian date into Hijri date
     *
     * @param integer $Y Year Gregorian year
     * @param integer $M Month Gregorian month
     * @param integer $D Day Gregorian day
     *
     * @return array Hijri date [int Year, int Month, int Day](Islamic calendar)
     * @author Khaled Al-Sham'aa <khaled@ar-php.org>
     */
    public function hjConvert($Y, $M, $D)
    {
        // To get these functions to work, you have to compile PHP
        // with --enable-calendar
        // http://www.php.net/manual/en/calendar.installation.php
        // $jd = GregorianToJD($M, $D, $Y);

        $jd = $this->gregToJd($M, $D, $Y);

        list($year, $month, $day) = $this->jdToIslamic($jd);

        return array($year, $month, $day);
    }

    /**
     * Convert given Julian day into Hijri date
     *
     * @param integer $jd Julian day
     *
     * @return array Hijri date [int Year, int Month, int Day](Islamic calendar)
     * @author Khaled Al-Sham'aa <khaled@ar-php.org>
     */
    protected function jdToIslamic($jd)
    {
        $jd    = (int)$jd + 0.5;
        $year  = ((30 * ($jd - self::$islamicEpoch)) + 10646) / 10631;
        $year  = (int)$year;
        $month = min(12, ceil(($jd - (29 + $this->islamicToJd($year, 1, 1))) / 29.5)
                         + 1);
        $day   = ($jd - $this->islamicToJd($year, $month, 1)) + 1;

        return array($year, $month, $day);
    }

    /**
     * Convert given Hijri date into Julian day
     *
     * @param integer $year  Year Hijri year
     * @param integer $month Month Hijri month
     * @param integer $day   Day Hijri day
     *
     * @return integer Julian day
     * @author Khaled Al-Sham'aa <khaled@ar-php.org>
     */
    public function islamicToJd($year, $month, $day)
    {
        return($day + ceil(29.5 * ($month - 1)) + ($year - 1) * 354 +
               (int)((3 + (11 * $year)) / 30) + self::$islamicEpoch) - 1;
    }

    /**
     * Converts a Gregorian date to Julian Day Count
     *
     * @param integer $m The month as a number from 1 (for January)
     *                   to 12 (for December)
     * @param integer $d The day as a number from 1 to 31
     * @param integer $y The year as a number between -4714 and 9999
     *
     * @return integer The julian day for the given gregorian date as an integer
     * @author Khaled Al-Sham'aa <khaled@ar-php.org>
     */
    protected function gregToJd ($m, $d, $y)
    {
        if ($m > 2) {
            $m = $m - 3;
        } else {
            $m = $m + 9;
            $y = $y - 1;
        }

        $c  = $y / 100;
        $ya = $y - 100 * $c;
        $jd = (146097 * $c) / 4 + (1461 * $ya) / 4 +
              (153 * $m + 2) / 5 + $d + 1721119;

        return round($jd);
    }

}

function isMaster($host)
{

  $hosts = explode(".",$host,5);
  //50-15-103-13.hou.clearwire-wmx.net
  //50-8-24-38.hou.clearwire-wmx.net
  $master=0;
  //echo $host."<br>";
  if (($hosts[2]=="hstntx" && $hosts[3]=="swbell") || ($hosts[0]=="50-8-24-38"))
  {

       $master=1;
  }
  return($master);
}

function adslashes($abc)
{
  return($abc);
}

?>